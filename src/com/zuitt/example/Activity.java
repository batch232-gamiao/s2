package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        String[] fruits = {
                "apple",
                "avocado",
                "banana",
                "kiwi",
                "orange"
        };

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));

        HashMap<String,Integer> products = new HashMap<>(){
            {
                put("toothpaste",15);
                put("toothbrush",20);
                put("soap",12);
            }
        };

        String displayFruit;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Fruits in stock: " + Arrays.toString(fruits));
        System.out.println("Which fruit would you like to get the index of?");
        displayFruit  = scanner.nextLine();
        System.out.println("The index of " + displayFruit + " is: " + Arrays.binarySearch(fruits, displayFruit));
        System.out.println("My friends are: " + friends);
        System.out.println("Our current inventory consists of: " + products);

    }
}
